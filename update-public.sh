#!/bin/bash
last_commit="$(git log --oneline -n 1 --pretty=%B | tr -d "\n")"
auto_push="true"

cp -r images public/
cp -r scripts public/
cp -r styles public/
cp -r html public/
cp -r fonts public/
cp index.html public/

if [ $auto_push == "true" ]
then
    git add * .gitlab-ci.yml
    # *...* = If string contains the substring
    if [[ $last_commit == *"Updated public directory"* ]]
    then
        git commit -S -m "Updated public directory"
    else
        git commit -S -m "$last_commit (Updated public directory)"
    fi
    git push -u origin main
fi
