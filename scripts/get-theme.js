document.addEventListener("DOMContentLoaded", function() {
  if(document.cookie != '') {
    document.documentElement.setAttribute("data-theme", document.cookie);
    if(document.cookie == "mode=dark") {
      var themeSwitcher = document.getElementById("switch");
      themeSwitcher.click();
      document.cookie = "mode=dark ;path=/";
      document.documentElement.setAttribute("data-theme", document.cookie);
    }
  } else {
    document.cookie = "mode=light ;path=/";
    document.documentElement.setAttribute("data-theme", document.cookie)
  }
})
